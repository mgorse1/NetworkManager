// SPDX-License-Identifier: LGPL-2.1+
/*
 * Copyright (C) 2017, 2018 Red Hat, Inc.
 */

#ifndef __NM_JSON_H__
#define __NM_JSON_H__

gboolean nm_jansson_load (void);

#ifndef NM_JANSSON_C
#include "nm-glib-aux/nm-jansson.h"

#define json_object_iter_value  (*_nm_jansson_json_object_iter_value)
extern json_t * json_object_iter_value(void *iter);
#define json_object_key_to_iter (*_nm_jansson_json_object_key_to_iter)
extern void *json_object_key_to_iter(const char *key);
#define json_integer            (*_nm_jansson_json_integer)
extern json_t *json_integer(json_int_t value);
#define json_object_del         (*_nm_jansson_json_object_del)
extern int json_object_del(json_t *object, const char *key);
#define json_array_get          (*_nm_jansson_json_array_get)
extern json_t *json_array_get(const json_t *array, size_t index) JANSSON_ATTRS(warn_unused_result);
#define json_array_size         (*_nm_jansson_json_array_size)
extern size_t json_array_size(const json_t *array);
#define json_array_append_new   (*_nm_jansson_json_array_append_new)
extern int json_array_append_new(json_t *array, json_t *value);
#define json_string             (*_nm_jansson_json_string)
extern json_t *json_string(const char *value);
#define json_object_iter_next   (*_nm_jansson_json_object_iter_next)
extern void *json_object_iter_next(json_t *object, void *iter);
#define json_loads              (*_nm_jansson_json_loads)
extern json_t *json_loads(const char *input, size_t flags, json_error_t *error) JANSSON_ATTRS(warn_unused_result);
#define json_dumps              (*_nm_jansson_json_dumps)
extern char *json_dumps(const json_t *json, size_t flags) JANSSON_ATTRS(warn_unused_result);
#define json_object_iter_key    (*_nm_jansson_json_object_iter_key)
extern const char *json_object_iter_key(void *iter);
#define json_object             (*_nm_jansson_json_object)
extern json_t *json_object(void);
#define json_object_get         (*_nm_jansson_json_object_get)
extern json_t *json_object_get(const json_t *object, const char *key) JANSSON_ATTRS(warn_unused_result);
#define json_array              (*_nm_jansson_json_array)
extern json_t *json_array(void);
#define json_false              (*_nm_jansson_json_false)
extern json_t *json_false(void);
#define json_delete             (*_nm_jansson_json_delete)
extern void json_delete(json_t *json);
#define json_true               (*_nm_jansson_json_true)
extern json_t *json_true(void);
#define json_object_size        (*_nm_jansson_json_object_size)
extern size_t json_object_size(const json_t *object);
#define json_object_set_new     (*_nm_jansson_json_object_set_new)
extern int json_object_set_new(json_t *object, const char *key, json_t *value);
#define json_object_iter        (*_nm_jansson_json_object_iter)
extern void *json_object_iter(json_t *object);
#define json_integer_value      (*_nm_jansson_json_integer_value)
extern json_int_t json_integer_value(const json_t *integer);
#define json_string_value       (*_nm_jansson_json_string_value)
extern const char *json_string_value(const json_t *string);
#endif

#endif /* __NM_JSON_H__ */
